export const numberPad = function(num, maxLength = 2) {
    return `${num}`.padStart(maxLength, '0')
  }

export const msToTimecode = function (milliseconds, padStart) {
  const asSeconds = milliseconds / 1000

  let hours = 0
  let minutes = Math.floor(asSeconds / 60)
  const seconds = Math.floor(asSeconds % 60)

  if (minutes > 59) {
    hours = Math.floor(minutes / 60)
    minutes %= 60
  }

  return hours
    ? `${padStart ? numberPad(hours) : hours}:${numberPad(minutes)}:${numberPad(seconds)}`
    : `${padStart ? numberPad(minutes) : minutes}:${numberPad(seconds)}`
}

export const firstServerErrorMsg = function(e) {
  if (e.response.data && e.response.data.errors) {
    for (const i in e.response.data.errors) {
      if (e.response.data.errors[i][0]) {
        return e.response.data.errors[i][0]
      }
    }
  }
  return null
}
