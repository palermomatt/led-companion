import Device from '~/models/Device'
import Tag from '~/models/Tag'
import XschedulePlaylist from '~/models/XschedulePlaylist'

export const state = () => ({
  devices: [],
  tags: [],
  xschedulePlaylists: [],
})

export const getters = {
  devices: (state) => {
    return state.devices.map((i) => new Device(i))
  },
  tags: (state) => {
    return state.tags.map((i) => new Tag(i))
  },
  xschedulePlaylists: (state) => {
    return state.xschedulePlaylists.map((i) => new XschedulePlaylist(i))
  },
}

export const actions = {
  async nuxtServerInit({ dispatch }) {
    await dispatch('fetchAll')
  },

  async fetchAll({ dispatch }) {
    await Promise.all([
      await dispatch('fetchDevices'),
      await dispatch('fetchTags'),
      await dispatch('fetchXschedulePlaylists'),
    ])
  },

  async fetchDevices({ commit }) {
    try {
      const devices = await Device.include('meta').$get()
      commit('setDevices', devices)
    } catch (e) {
      console.log('fetchDevices error', e)
    }
  },

  async fetchTags({ commit }) {
    try {
      const tags = await Tag.$get()
      commit('setTags', tags)
    } catch (e) {
      console.log('fetchTags error', e)
    }
  },

  async fetchXschedulePlaylists({ commit }) {
    try {
      const playlists = await XschedulePlaylist.include('steps').$get()
      commit('setXschedulePlaylists', playlists)
    } catch (e) {
      console.log('fetchXschedulePlaylists error', e)
    }
  },
}

export const mutations = {
  setDevices(state, devices) {
    state.devices = devices
  },
  setTags(state, tags) {
    state.tags = tags
  },
  setXschedulePlaylists(state, playlists) {
    state.xschedulePlaylists = playlists
  },
}
