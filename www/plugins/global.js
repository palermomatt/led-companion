import Vue from 'vue'
import btn from '@/components/ui/Btn.vue'
import icon from '@/components/ui/Icon.vue'
import modal from '@/components/ui/Modal.vue'
import PortalVue from 'portal-vue'

Vue.component('Btn', btn)
Vue.component('Icon', icon)
Vue.component('Modal', modal)
Vue.use(PortalVue)