import { Model as BaseModel } from 'vue-api-query'

export default class Model extends BaseModel {

  // Define a base url for a REST API
  baseURL() {
    // Get from $axios
    return this.$http.defaults.baseURL
  }

  // Implement a default request method
  request(config) {
    return this.$http.request(config)
  }

  toJSON() {
    return Object.getOwnPropertyNames(this).reduce((a, b) => {
        a[b] = this[b];
        return a;
    }, {});
  }
}