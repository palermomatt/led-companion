import Model from './Model'
import Command from '~/models/Device/Command'
import Win from '~/models/Device/Win'
import Restore from '~/models/Device/Restore'
import Snapshot from '~/models/Device/Snapshot'
export default class Device extends Model {
  resource() {
    return 'devices'
  }

  commands() {
    return this.hasMany(Command)
  }

  wins() {
    return this.hasMany(Win)
  }
  
  restores() {
    return this.hasMany(Restore)
  }

  snapshots() {
    return this.hasMany(Snapshot)
  }

  get presetPresets() {
    if (!this.presets) return []
    return this.presets.filter((preset) => !preset.is_playlist)
  }

  get presetPlaylists() {
    if (!this.presets) return []
    return this.presets.filter((preset) => preset.is_playlist)
  }

  ping(ip) {
    return this.custom(`devices/ping/${ip}`)
  }

  async loadPreset(idx) {
    try {
      await this.commands().attach({ ps: idx })
    } catch (e) {
      console.log('error', e)
    }
  }

  async powerToggle() {
    try {
      await this.commands().attach({ on: 't' })
    } catch (e) {
      console.log('error', e)
    }
  }
}
