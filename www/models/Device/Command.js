import Model from '~/models/Model'
import Device from '~/models/Device'

export default class Command extends Model {
  resource() {
    return 'cmd'
  }

  device() {
    return this.belongsTo(Device)
  }
}
