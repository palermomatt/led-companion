import Model from '~/models/Model'
import Device from '~/models/Device'

export default class Command extends Model {
  resource() {
    return 'win'
  }

  device() {
    return this.belongsTo(Device)
  }
}
