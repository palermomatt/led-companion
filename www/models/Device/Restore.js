import Model from '~/models/Model'
import Device from '~/models/Device'

export default class Restores extends Model {
  resource() {
    return 'restores'
  }

  device() {
    return this.belongsTo(Device)
  }
}
