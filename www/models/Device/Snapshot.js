import Model from '~/models/Model'
import Device from '~/models/Device'

export default class Snapshot extends Model {
  resource() {
    return 'snapshots'
  }

  device() {
    return this.belongsTo(Device)
  }
}
