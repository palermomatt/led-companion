import Model from './Model'
import { msToTimecode } from '~/utils'

export default class XshedulePlaylist extends Model {
  resource() {
    return 'xschedule/playlists'
  }
  
  get runtimecode() {
    return msToTimecode(this.runtime)
  }
}
