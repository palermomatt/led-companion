FROM php:7.4.14-fpm-alpine

RUN apk update && apk add build-base libxml2-dev libzip-dev

RUN apk add postgresql postgresql-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql

RUN docker-php-ext-install pdo_mysql dom zip bcmath

RUN apk add --update --no-cache \
    freetype-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    && docker-php-ext-install -j"$(getconf _NPROCESSORS_ONLN)" iconv \
    && docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ \
    && docker-php-ext-install -j"$(getconf _NPROCESSORS_ONLN)" gd

RUN apk add --no-cache autoconf gcc g++ imagemagick imagemagick-libs imagemagick-dev libtool make \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-install exif \
    && apk del autoconf g++ libtool make \
    && rm -rf /tmp/* /var/cache/apk/*   

RUN docker-php-source extract \
    && curl -L -o /tmp/redis.tar.gz https://github.com/phpredis/phpredis/archive/5.0.2.tar.gz \
    && tar xfz /tmp/redis.tar.gz \
    && rm -r /tmp/redis.tar.gz \
    && mv phpredis-5.0.2 /usr/src/php/ext/redis \
    && docker-php-ext-install redis

RUN apk add --no-cache wget curl git

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

RUN apk add --no-cache nodejs npm yarn

RUN curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
RUN curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar
RUN chmod +x *.phar
RUN mv phpcbf.phar  /usr/bin/phpcbf
RUN mv phpcs.phar   /usr/bin/phpcs

RUN apk --no-cache upgrade && apk add --no-cache chromium
ENV PUPPETEER_SKIP_DOWNLOAD=1
RUN npm install puppeteer --global

# OPcache defaults
ENV PHP_OPCACHE_ENABLE="1"
ENV PHP_OPCACHE_MEMORY_CONSUMPTION="192"
ENV PHP_OPCACHE_MAX_ACCELERATED_FILES="20000"
ENV PHP_OPCACHE_REVALIDATE_FREQUENCY="0"
ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS="0"
ENV PHP_OPCACHE_INTERNED_STRINGS_BUFFER="16"
ENV PHP_OPCACHE_FAST_SHUTDOWN="1"

# Install opcache and add the configuration file
RUN docker-php-ext-install opcache
ADD ./_docker/php/opcache.ini "$PHP_INI_DIR/conf.d/opcache.ini"

# PHP-FPM defaults
ENV PHP_FPM_PM="dynamic"
ENV PHP_FPM_MAX_CHILDREN="200"
ENV PHP_FPM_START_SERVERS="20"
ENV PHP_FPM_MIN_SPARE_SERVERS="15"
ENV PHP_FPM_MAX_SPARE_SERVERS="30"
ENV PHP_FPM_MAX_REQUESTS="1000"

WORKDIR /api

# Copy the PHP-FPM configuration file
COPY ./_docker/php/www.conf /usr/local/etc/php-fpm.d/www.conf

COPY ./api /api

# Install dependencies
RUN composer install --no-dev --optimize-autoloader --no-ansi --no-interaction --no-progress
RUN rm -rf ./auth.json

RUN chmod -R 0777 /api/storage

COPY ./_docker/php.start /usr/local/bin/start
RUN chmod +x /usr/local/bin/start
CMD ["/usr/local/bin/start"]