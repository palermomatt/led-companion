FROM redis:3.2
COPY ./_docker/redis.conf /usr/local/etc/redis/redis.conf
ENTRYPOINT [ "redis-server", "/usr/local/etc/redis/redis.conf" ]