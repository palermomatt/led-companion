<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\DevicesController;
use App\Http\Controllers\TagScansController;
use App\Http\Controllers\DeviceRestoresController;
use App\Http\Controllers\DeviceSnapshotsController;
use App\Http\Controllers\XschedulePlaylistsController;

Route::apiResources([
    'devices' => DevicesController::class,
    'devices.snapshots' => DeviceSnapshotsController::class,
    'devices.restores' => DeviceRestoresController::class,

    'tags' => TagsController::class,
    'xschedule/playlists' => XschedulePlaylistsController::class,
]);
Route::get('devices/ping/{ipAddress}', [DevicesController::class, 'ping']);
Route::post('devices/{device}/cmd', [DevicesController::class, 'cmd']);
Route::post('devices/{device}/win', [DevicesController::class, 'win']);
Route::post('tags/scans', [TagScansController::class, 'store']);