<?php

use App\Jobs\DeviceDiscover;
use App\Actions\CreateDeviceSnapshot;
use App\Actions\RestoreDeviceSnapshot;
use Illuminate\Support\Facades\Artisan;

Artisan::command('test', function() {
    DeviceDiscover::dispatch(\App\Models\Device::find(5));
});

Artisan::command('backup', function() {
    (new CreateDeviceSnapshot())->execute(\App\Models\Device::find(5));
});

Artisan::command('restore', function() {
    (new RestoreDeviceSnapshot())->execute(\App\Models\Device::find(5), \App\Models\Snapshot::find(21));
});
