<?php

return [
    'xschedule_url' => env('LC_XSCHEDULE_URL', 'http://localhost'),
    'xschedule_scan_interval' => env('LC_XSCHEDULE_SCAN_INTERVAL', 10000),

    'device_scan_interval' => env('LC_DEVICE_SCAN_INTERVAL', 10000),
];
