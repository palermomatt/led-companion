<?php

use App\Models\Device;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_metas', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Device::class)->cascadeOnUpdate()->cascadeOnDelete()->nullable();
            $table->string('name')->nullable()->index();
            $table->unsignedInteger('num_leds')->default(0)->index();
            $table->string('ip')->nullable()->index();
            $table->string('mac')->nullable()->index();
            $table->string('wifi_bssid')->nullable()->index();
            $table->unsignedInteger('wifi_signal')->default(0)->index();
            $table->string('version')->nullable();
            $table->boolean('on')->default(0)->index();
            $table->unsignedInteger('uptime')->default(0);
            $table->unsignedInteger('brightness')->default(0)->index();
            $table->string('sync_protocol')->nullable();
            $table->longText('raw')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_metas');
    }
}
