<?php

use App\Models\Device;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicePresetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_presets', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Device::class)->cascadeOnUpdate()->cascadeOnDelete()->nullable();
            $table->unsignedInteger('idx')->default(0)->index();
            $table->string('name')->nullable()->index();
            $table->string('quick_label')->nullable();
            $table->boolean('is_playlist')->default(0)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_presets');
    }
}
