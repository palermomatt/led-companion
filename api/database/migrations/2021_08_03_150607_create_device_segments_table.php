<?php

use App\Models\Device;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeviceSegmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_segments', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Device::class)->cascadeOnUpdate()->cascadeOnDelete()->nullable();
            $table->unsignedInteger('idx')->default(0)->index();
            $table->unsignedInteger('start')->default(0)->index();
            $table->unsignedInteger('stop')->default(0)->index();
            $table->unsignedInteger('num_leds')->default(0)->index();
            $table->boolean('on')->default(0)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_segments');
    }
}
