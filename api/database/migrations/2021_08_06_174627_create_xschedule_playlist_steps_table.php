<?php

use App\Models\XschedulePlaylist;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXschedulePlaylistStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xschedule_playlist_steps', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(XschedulePlaylist::class)->cascadeOnUpdate()->cascadeOnDelete()->nullable();
            $table->unsignedInteger('idx')->default(0)->index();
            $table->string('name')->nullable()->index();
            $table->unsignedInteger('runtime')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xschedule_playlist_steps');
    }
}
