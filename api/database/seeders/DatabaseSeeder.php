<?php

namespace Database\Seeders;

use App\Models\Device;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Device::insert([
            [
                'name' => 'fairy-leds',
                'type' => 'wled',
                'version' => 1,
                'host' => '10.0.0.120',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ], [
                'name' => 'glow-plug',
                'type' => 'wled',
                'version' => 1,
                'host' => '10.0.0.175',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ], [
                'name' => 'dino_light',
                'type' => 'wled',
                'version' => 1,
                'host' => '10.0.0.62',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ], [
                'name' => 'desklamp',
                'type' => 'wled',
                'version' => 1,
                'host' => '10.0.0.162',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ], 
        ]);
    }
}
