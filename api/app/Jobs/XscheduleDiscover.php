<?php

namespace App\Jobs;

use App\Services\Xschedule;
use Illuminate\Support\Arr;
use Illuminate\Bus\Queueable;
use App\Models\XschedulePlaylist;
use Spatie\Browsershot\Browsershot;
use Illuminate\Support\Facades\Http;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class XscheduleDiscover implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct() 
    {
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $service = (new Xschedule(Http::baseUrl(config('ledcompanion.xschedule_url'))));

        $xschedulePlaylists = $service->getPlaylists();
        $playlistIds = [];
        if($xschedulePlaylists && is_array($xschedulePlaylists['playlists'])) {
            foreach($xschedulePlaylists['playlists'] as $i) {
                $playlist = XschedulePlaylist::firstOrCreate([
                    'idx' => $i['id'],
                ], [
                    'name' => Arr::get($i, 'name'),
                    'runtime' => Arr::get($i, 'lengthms', null),
                ]);
                $playlistIds[] = $playlist->id;

                $steps = $service->getPlaylistSteps($playlist->name);
                if($steps && is_array($steps['steps'])) {
                    $stepIds = [];
                    foreach($steps['steps'] as $s) {
                        $step = $playlist->steps()->firstOrCreate([
                            'xschedule_playlist_id' => $playlist->id,
                            'idx' => $s['id'],
                        ], [
                            'name' => Arr::get($s, 'name'),
                            'runtime' => Arr::get($s, 'lengthms', null),
                        ]);
                        $stepIds[] = $step->id;
                    }
                }
                $playlist->steps()->whereNotIn('id', $stepIds)->delete();
            }
        }
        XschedulePlaylist::whereNotIn('id', $playlistIds)->delete();
    }

    protected function scanSettings() {
        $settings = [
            'leds',
            'wifi',
            'ui',
            'sync',
            'time',
            'sec',
        ];

        $settingIds = [];
        foreach($settings as $type) {
            echo 'Scanning device settings ['.$type.']...'."\n";
            $data = [];
            try {
                $data = json_decode(
                    Browsershot::url($this->device->url.'/settings/'.$type)
                        ->setChromePath('/usr/bin/chromium-browser')
                        ->noSandbox()
                        ->timeout(5)
                        ->waitUntilNetworkIdle()
                        ->evaluate("var params = {};
                        for(var i = 0; i < document.getElementById('form_s').elements.length; i++)
                        {
                            var fieldName = document.getElementById('form_s').elements[i].name;
                            var fieldValue = document.getElementById('form_s').elements[i].value;
                            if(!fieldName.length) continue;
                            params[fieldName] = fieldValue;
                        }
                        JSON.stringify(params);")
                    , true);
            }
            catch(\Exception $e) {
                echo $e->getMessage()."\n";
            }
            $setting = $this->device->settings()->firstOrCreate([
                'device_id' => $this->device->id,
                'type' => $type,
            ], [
                'raw' => $data,
            ]);
            $settingIds[] = $setting->id;
        }
        $this->device->settings()->whereNotIn('id', $settingIds)->delete();
    }
}
