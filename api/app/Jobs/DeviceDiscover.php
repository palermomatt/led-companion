<?php

namespace App\Jobs;

use App\Actions\CreateDeviceSnapshot;
use Carbon\Carbon;
use App\Models\Device;
use App\Models\DeviceSetting;
use Illuminate\Support\Arr;
use Illuminate\Bus\Queueable;
use Spatie\Browsershot\Browsershot;
use Illuminate\Support\Facades\Http;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class DeviceDiscover implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $device;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Device $device) 
    {
        $this->device = $device;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateDeviceSnapshot $createDeviceSnapshot)
    {
        // Create snapshot first
        try {
            $createDeviceSnapshot->execute($this->device);
        }
        catch(\Exception $e) {}

        $this->device->update(['is_scanning' => true]);

        echo "\n\n".'Scanning controller: '.$this->device->name.'...'."\n";
        try {
            $config = Http::timeout(4)->get($this->device->url.'/edit?edit=cfg.json')->throw()->json();
            $presets = Http::timeout(4)->get($this->device->url.'/edit?edit=presets.json')->throw()->json();
            $raw = Http::timeout(2)->get($this->device->url.'/json')->json();
            $files = Http::timeout(4)->get($this->device->url.'/edit?list=%2F')->json();
            $this->device->is_online = true;

            // All settings
            $settingIds = [];
            if(is_array($files)) {
                foreach($files as $file) {
                    if($file['type'] !== 'file') continue;
                    $data = Http::timeout(4)->get($this->device->url.'/edit?edit='.$file['name'])->body();
                    $setting = $this->device->settings()->updateOrCreate([
                        'device_id' => $this->device->id,
                        'type' => $file['name'],
                    ], [
                        'raw' => $data,
                    ]);
                    $settingIds[] = $setting->id;
                }
                $this->device->settings()->whereNotIn('id', $settingIds)->delete();
            }
            
            // Meta
            $meta = $this->device->meta()->updateOrCreate([
                'device_id' => $this->device->id,
            ], [
                'name' => Arr::get($raw, 'info.name', null),
                'ip' => Arr::get($raw, 'info.ip', null),
                'mac' => Arr::get($raw, 'info.mac', null),
                'wifi_bssid' => Arr::get($raw, 'info.wifi.bssid', null),
                'wifi_signal' => Arr::get($raw, 'info.wifi.signal', null),
                'version' => Arr::get($raw, 'info.ver', null),
                'on' => Arr::get($raw, 'state.on', 0),
                'num_leds' => Arr::get($raw, 'info.leds.count', 0),
                'uptime' => Arr::get($raw, 'info.uptime', null),
                'brightness' => Arr::get($raw, 'state.bri', null),
                'sync_protocol' => Arr::get($config, 'if.live.port', null),
                'raw' => $raw,
            ]);
            if(!$this->device->name && !empty($meta->name)) $this->device->name = $meta->name;

            // Segments
            $segmentIds = [];
            foreach(Arr::get($raw, 'state.seg', []) as $i) {
                $segment = $this->device->segments()->updateOrCreate([
                    'device_id' => $this->device->id,
                    'idx' => Arr::get($i, 'id', null),
                ], [
                    'start' => Arr::get($i, 'start', null),
                    'stop' => Arr::get($i, 'stop', null),
                    'num_leds' => Arr::get($i, 'len', null),
                    'on' => Arr::get($i, 'on', null),
                ]);
                $segmentIds[] = $segment->id;
            }
            $this->device->segments()->whereNotIn('id', $segmentIds)->delete();

            // Effects
            $effectIds = [];
            foreach(Arr::get($raw, 'effects', []) as $k => $v) {
                $effect = $this->device->effects()->updateOrCreate([
                    'device_id' => $this->device->id,
                    'idx' => $k,
                ], [
                    'name' => $v,
                ]);
                $effectIds[] = $effect->id;
            }
            $this->device->effects()->whereNotIn('id', $effectIds)->delete();

            // Presets
            $presetIds = [];
            foreach($presets as $idx => $i) {
                if(!$i) continue;
                $preset = $this->device->presets()->updateOrCreate([
                    'device_id' => $this->device->id,
                    'idx' => $idx,
                ], [
                    'name' => Arr::get($i, 'n', null),
                    'quick_label' => Arr::get($i, 'ql', null),
                    'is_playlist' => !!Arr::get($i, 'playlist', null),
                ]);
                $presetIds[] = $preset->id;
            }
            $this->device->presets()->whereNotIn('id', $presetIds)->delete();
        }
        catch(\Exception $e) {
            echo $e->getMessage()."\n";
        }

        $this->device->is_scanning = false;
        $this->device->scanned_at = Carbon::now();
        $this->device->save();
    }
}
