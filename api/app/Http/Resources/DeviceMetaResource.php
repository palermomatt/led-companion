<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeviceMetaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {  
        return [
            'id' => $this->id,
            'device_id' => $this->device_id,
            'name' => $this->name,
            'num_leds' => $this->num_leds,
            'ip' => $this->ip,
            'mac' => $this->mac,
            'wifi_bssid' => $this->wifi_bssid,
            'wifi_signal' => $this->wifi_signal,
            'version' => $this->version,
            'uptime' => $this->uptime,
            'brightness' => $this->brightness,
            'sync_protocol' => $this->sync_protocol,
            'sync_protocol_value' => $this->sync_protocol_value,
            'on' => $this->on,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
