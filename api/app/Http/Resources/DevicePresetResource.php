<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DevicePresetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {  
        return [
            'id' => $this->id,
            'device_id' => $this->device_id,
            'idx' => $this->idx,
            'name' => $this->name,
            'quick_label' => $this->quick_label,
            'is_playlist' => $this->is_playlist,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
