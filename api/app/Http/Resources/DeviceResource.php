<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeviceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {  
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'host' => $this->host,
            'url' => $this->url,
            'is_online' => $this->is_online,
            'meta' => (new DeviceMetaResource($this->whenLoaded('meta'))),
            'segments' => DeviceSegmentResource::collection($this->whenLoaded('segments')),
            'effects' => DeviceEffectResource::collection($this->whenLoaded('effects')),
            'presets' => DevicePresetResource::collection($this->whenLoaded('presets')),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
