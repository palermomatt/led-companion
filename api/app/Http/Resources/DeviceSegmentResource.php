<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeviceSegmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {  
        return [
            'id' => $this->id,
            'device_id' => $this->device_id,
            'idx' => $this->idx,
            'start' => $this->start,
            'stop' => $this->stop,
            'num_leds' => $this->num_leds,
            'on' => $this->on,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
