<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class XschedulePlaylistResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {  
        return [
            'id' => $this->id,
            'idx' => $this->idx,
            'name' => $this->name,
            'runtime' => $this->runtime,
            'steps' => XschedulePlaylistStepResource::collection($this->whenLoaded('steps')),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
