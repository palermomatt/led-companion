<?php

namespace App\Http\Controllers;

use App\Http\Queries\XschedulePlaylistQuery;
use App\Http\Resources\XschedulePlaylistResource;

class XschedulePlaylistsController extends Controller
{
    public function index(XschedulePlaylistQuery $query) {
        return XschedulePlaylistResource::collection($query->paginate());
    }

    public function show(XschedulePlaylistQuery $query, $id) {
        $playlist = $query->findOrFail($id);
        return new XschedulePlaylistResource($playlist);
    }
}
