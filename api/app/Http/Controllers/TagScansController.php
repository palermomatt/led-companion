<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\TagsStoreUpdate;

class TagScansController extends Controller
{
    public function store(TagsStoreUpdate $request) {
        $tag = Tag::updateOrCreate([
            'tag_id' => $request->get('tag_id')
        ]);

        $tagScan = $tag->scans()->create([
            'tag_id' => $tag->id,
            'scanner_id' => $request->get('scanner_id', null),
        ]);

        if($tag->tag_id === '04-43-44-01-07-3F-03') {
            Http::get('http://10.0.0.10/xScheduleCommand', [
                'Command' => 'Play playlist step',
                'Parameters' => 'OPS,carol_of_the_bells',
            ]);
        }
        else if($tag->tag_id === 'e2cc7296-c2ba-47b2-9b62-c634b0a998c8') {
            Http::get('http://10.0.0.10/xScheduleCommand', [
                'Command' => 'Play playlist step',
                'Parameters' => 'OPS,its_beginning_to_look_a_lot_like_christmas',
            ]);
        }
        else if($tag->tag_id === '1f4f6134-0179-42ff-8e01-ec798bbfdc6d') {
            Http::get('http://10.0.0.10/xScheduleCommand', [
                'Command' => 'Play playlist step',
                'Parameters' => 'OPS,polar_express',
            ]);
        }
        else if($tag->tag_id === '4f148941-7443-49e2-a105-a2c190783260') {
            Http::get('http://10.0.0.10/xScheduleCommand', [
                'Command' => 'Play playlist step',
                'Parameters' => 'OPS,nutrocker',
            ]);
        }
        else if($tag->tag_id === '74-10-37-94') {
            Http::get('http://10.0.0.10/xScheduleCommand', [
                'Command' => 'Play playlist step',
                'Parameters' => 'OPS,nutcracker_suite',
            ]);
        }
        else if($tag->tag_id === '04-43-90-01-92-3F-03') {
            Http::get('http://10.0.0.10/xScheduleCommand', [
                'Command' => 'Play playlist step',
                'Parameters' => 'OPS,music_box_dancer',
            ]);
        }
        return $request->all();
    }
}
