<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Actions\CreateDeviceSnapshot;
use App\Http\Resources\DeviceSnapshotResource;
use App\Http\Queries\DeviceSnapshotQuery;
use App\Http\Requests\DeviceSnapshotsStoreUpdate;

class DeviceSnapshotsController extends Controller
{
    public function index(Device $device) {
        return DeviceSnapshotResource::collection((new DeviceSnapshotQuery($device))->get());
    }

    public function store(DeviceSnapshotsStoreUpdate $request, Device $device, CreateDeviceSnapshot $createDeviceSnapshot) {
        $snapshot = $createDeviceSnapshot->execute($device, 'manual', $request->comment);
        return new DeviceSnapshotResource($snapshot);
    }
}
