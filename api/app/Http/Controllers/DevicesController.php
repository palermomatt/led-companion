<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Actions\WinDevice;
use Illuminate\Http\Request;
use App\Actions\CommandDevice;
use App\Actions\PingDevice;
use App\Http\Queries\DeviceQuery;
use App\Http\Requests\DeviceStoreUpdate;
use App\Http\Resources\DeviceResource;

class DevicesController extends Controller
{
    public function index(DeviceQuery $query) {
        return DeviceResource::collection($query->paginate());
    }

    public function show(DeviceQuery $query, $id) {
        $device = $query->findOrFail($id);
        return new DeviceResource($device);
    }

    public function store(DeviceStoreUpdate $request) {
        $device = Device::create($request->validated());
        return new DeviceResource($device);
    }

    public function destroy(Device $device) {
        $device->delete();
    }

    public function cmd(Request $request, Device $device, CommandDevice $commandDevice) {
        $commandDevice->execute($device, $request->all());
    }

    public function win(Request $request, Device $device, WinDevice $winDevice) {
        $winDevice->execute($device, $request->all());
    }

    public function ping(Request $request, $ipAddress, PingDevice $pingDevice) {
        return $pingDevice->execute($ipAddress);
    }
}
