<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Models\Snapshot;
use App\Actions\RestoreDeviceSnapshot;
use App\Http\Requests\DeviceRestoresStoreUpdate;

class DeviceRestoresController extends Controller
{
    public function store(DeviceRestoresStoreUpdate $request, Device $device, RestoreDeviceSnapshot $restoreDeviceSnapshot) {
        $restoreDeviceSnapshot->execute($device, Snapshot::find($request->snapshot_id));
    }
}
