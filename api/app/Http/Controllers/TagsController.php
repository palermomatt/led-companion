<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Services\Xschedule;
use Illuminate\Support\Facades\Http;

class TagsController extends Controller
{
    public function index() {
        return Tag::get();
    }

    public function show($id) {
        return Tag::findOrFail($id);
    }
}
