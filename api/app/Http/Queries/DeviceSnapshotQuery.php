<?php

namespace App\Http\Queries;

use App\Models\Device;
use Spatie\QueryBuilder\QueryBuilder;

class DeviceSnapshotQuery extends QueryBuilder
{
    public function __construct(Device $device)
    {
        parent::__construct($device->snapshots()->getQuery());
        $this
            ->allowedSorts('created_at')
        ;
    }
}
