<?php

namespace App\Http\Queries;

use App\Models\XschedulePlaylist;
use Spatie\QueryBuilder\QueryBuilder;

class XschedulePlaylistQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(XschedulePlaylist::query());
        $this
            ->allowedIncludes('steps')
        ;
    }
}
