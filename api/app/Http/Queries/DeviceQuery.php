<?php

namespace App\Http\Queries;

use App\Models\Device;
use Spatie\QueryBuilder\QueryBuilder;

class DeviceQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Device::query());
        $this
            ->allowedIncludes('meta', 'segments', 'effects', 'presets', 'snapshots')
        ;
    }
}
