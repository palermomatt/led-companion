<?php
namespace App\Actions;

use App\Models\Device;
use Illuminate\Support\Facades\Http;

class WinDevice
{
    public function execute(Device $device, Array $params)
    {
        $response = Http::timeout(4)->get($device->url.'/win', $params)->json();
    }
}