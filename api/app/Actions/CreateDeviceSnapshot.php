<?php
namespace App\Actions;

use App\Models\Device;
use Illuminate\Support\Facades\Http;

class CreateDeviceSnapshot
{
    public function execute(Device $device, $type = null, $comment = null)
    {
        $backup = [];

        // Get backup data from device
        try {
            $files = Http::timeout(4)->get($device->url.'/edit?list=%2F')->json();
            if(is_array($files)) {
                foreach($files as $file) {
                    if($file['type'] !== 'file') continue;
                    $data = Http::timeout(4)->get($device->url.'/edit?edit='.$file['name'])->body();
                    $backup['files'][$file['name']] = $data;
                }
            }

            // If this is an auto snapshot, we don't need a duplicate of the most recent
            $lastSnapshot = $device->snapshots()->latest()->first();
            if(!$type && $lastSnapshot && json_encode($backup) === json_encode($lastSnapshot->data)) return $lastSnapshot;

            // Nothing to backup, likely not implemented in this firmware verson
            if(!$backup) throw new \Exception('No backup data available.');

            return $device->snapshots()->create([
                'data' => $backup,
                'type' => $type ?? 'auto',
                'comment' => $comment,
            ]);
        }
        catch(\Exception $e) {
            throw new \Exception('Error fetching controller snapshot data.');
        }
    }
}