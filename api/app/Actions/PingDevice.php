<?php
namespace App\Actions;

use Illuminate\Support\Facades\Http;

class PingDevice
{
    public function execute($ipAddress)
    {
        return Http::timeout(4)->get('http://'.$ipAddress.'/json')->throw()->json();
    }
}