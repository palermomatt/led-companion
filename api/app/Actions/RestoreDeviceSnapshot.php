<?php
namespace App\Actions;

use App\Models\Device;
use App\Models\Snapshot;
use Illuminate\Support\Facades\Http;

class RestoreDeviceSnapshot
{
    public function execute(Device $device, Snapshot $snapshot, $reboot = true)
    {
        $restoredFiles = [];
        
        if($snapshot->data['files']) {
            foreach($snapshot->data['files'] as $name => $data) {
                $response = Http::retry(3, 500)->attach(
                    'data', $data, $name
                )->post($device->url.'/edit');
                if($response->failed()) dd('Restoring file ('.$name.') failed!');
                $restoredFiles[$name] = 1;
            }
        }

        // Check for files that currently exist on the controller, but don't exist in the snapshot, then delete those files
        $files = Http::timeout(4)->get($device->url.'/edit?list=%2F')->json();
        if(is_array($files)) {
            foreach($files as $file) {
                if(isset($restoredFiles[$file['name']])) continue;
                Http::asForm()->delete($device->url.'/edit', ['path' => $file['name']]);
            }
        }

        // Reboot
        if(!$reboot) return;
        $response = Http::post($device->url.'/json/state', ["rb" => true]);
    }
}