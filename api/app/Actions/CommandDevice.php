<?php
namespace App\Actions;

use App\Models\Device;
use Illuminate\Support\Facades\Http;

class CommandDevice
{
    public function execute(Device $device, Array $params)
    {
        $response = Http::timeout(4)->post($device->url.'/json', $params)->json();
    }
}