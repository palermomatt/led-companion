<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tag extends Model
{
    use HasFactory;

    public $guarded = ['id'];

    public function scans() {
        return $this->hasMany(TagScan::class);
    }
}
