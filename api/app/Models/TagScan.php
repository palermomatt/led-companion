<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TagScan extends Model
{
    use HasFactory;

    public $guarded = ['id'];

    public function tag() {
        return $this->belongsTo(Tag::class);
    }
}
