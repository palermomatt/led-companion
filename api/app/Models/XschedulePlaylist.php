<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class XschedulePlaylist extends Model
{
    use HasFactory;

    public $guarded = ['id'];

    public function steps() {
        return $this->hasMany(XschedulePlaylistStep::class);
    }
}
