<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Snapshot extends Model
{
    use HasFactory;

    public $guarded = ['id'];
    protected $casts = [
        'data' => 'array',
    ];
}
