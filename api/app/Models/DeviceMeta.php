<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DeviceMeta extends Model
{
    use HasFactory;

    public $table = 'device_metas';
    public $guarded = ['id'];
    protected $casts = [
        'num_leds' => 'integer',
        'uptime' => 'integer',
        'brightness' => 'integer',
        'wifi_signal' => 'integer',
        'on' => 'boolean',
        'raw' => 'array',
    ];

    public function device() {
        return $this->belongsTo(Device::class);
    }

    public function getSyncProtocolValueAttribute() {
        switch($this->sync_protocol) {
            case '4048': return 'DDP';
            case '5568': return 'E1.31 (sACN)';
            case '6454': return 'Art-Net';
        }
        return null;
    }
}
