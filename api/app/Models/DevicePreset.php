<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DevicePreset extends Model
{
    use HasFactory;

    public $guarded = ['id'];
    protected $casts = [
        'idx' => 'integer',
        'is_playlist' => 'boolean',
    ];

    public function device() {
        return $this->belongsTo(Device::class);
    }
}
