<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Device extends Model
{
    use HasFactory;

    public $guarded = ['id'];

    public function meta() {
        return $this->hasOne(DeviceMeta::class);
    }
    public function segments() {
        return $this->hasMany(DeviceSegment::class);
    }
    public function presets() {
        return $this->hasMany(DevicePreset::class);
    }
    public function effects() {
        return $this->hasMany(DeviceEffect::class);
    }
    public function settings() {
        return $this->hasMany(DeviceSetting::class);
    }
    public function snapshots() {
        return $this->morphMany(Snapshot::class, 'snappable');
    }

    public function scopeScannable($query, $interval = 5) {
        return $query->where('is_scanning', false)
                ->where(function($q) use($interval) {
                    return $q->whereNull('scanned_at')->orWhere('scanned_at', '<', Carbon::now()->subSeconds($interval)->toDateTimeString());
                });
    }

    public function getUrlAttribute() {
        return 'http://'.$this->host;
    }
}
