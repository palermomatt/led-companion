<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DeviceSegment extends Model
{
    use HasFactory;

    public $guarded = ['id'];
    protected $casts = [
        'idx' => 'integer',
        'start' => 'integer',
        'stop' => 'integer',
        'num_leds' => 'integer',
        'on' => 'boolean',
    ];

    public function device() {
        return $this->belongsTo(Device::class);
    }
}
