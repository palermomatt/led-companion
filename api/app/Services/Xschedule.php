<?php 
namespace App\Services;

class Xschedule {
    protected $client;

    public function __construct($client) {
        $this->client = $client;
    }

    public function getPlaylists() {
        return $this->client->get('xScheduleQuery', [
            'Query' => 'GetPlayLists'
        ])->json();
    }
    
    public function getPlaylistSteps($playlistName) {
        return $this->client->get('xScheduleQuery', [
            'Query' => 'GetPlayListSteps',
            'Parameters' => $playlistName,
        ])->json();
    }
}