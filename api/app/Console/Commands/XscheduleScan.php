<?php

namespace App\Console\Commands;

use App\Jobs\XscheduleDiscover;
use Illuminate\Console\Command;

class XscheduleScan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'xschedule:scan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan xschedule for playlists';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        while(1) {
            echo 'Scanning xschedule...'."\n";
            XscheduleDiscover::dispatch();
            echo 'Sleeping before next loop...'."\n";
            sleep(intval(config('ledcompanion.xschedule_scan_interval') / 1000));
        }
    }
}
