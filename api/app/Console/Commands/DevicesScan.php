<?php

namespace App\Console\Commands;

use App\Models\Device;
use App\Jobs\DeviceDiscover;
use Illuminate\Console\Command;

class DevicesScan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'devices:scan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan devices for updated states and settings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        while(1) {
            echo 'Finding led controllers to scan...'."\n";
            foreach(Device::scannable(intval(config('ledcompanion.device_scan_interval') / 1000))->get() as $device) {
                DeviceDiscover::dispatch($device);
            }
            
            echo 'Sleeping before next loop...'."\n";
            sleep(10);
        }
    }
}
